<?php
Namespace dgifford\Traits\Tests;


use dgifford\Traits\IteratorTrait;

class Mock implements \Iterator
{
    Use IteratorTrait;

    public function add( mixed $item ): void
    {
        $this->container[] = $item;
    }
}