<?php
Namespace dgifford\Traits\Tests;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class IteratorTraitTest extends \PHPUnit\Framework\TestCase
{
    public Mock $mock;
    
	public function setUp():void
	{
        parent::setUp();

		$this->mock = new Mock;

		$this->mock->add( 0 );
		$this->mock->add( 1 );
		$this->mock->add( 2 );
	}



	public function testIterator()
	{
		foreach( $this->mock as $key => $value )
		{
			$this->assertSame( $key, $value );
		}
	}



	public function testIteratorBreakInMiddle()
	{
		foreach( $this->mock as $key => $value )
		{
			$this->assertSame( $key, $value );

			if( $key == 1 )
			{
				break;
			}
		}

		$this->testIterator();
	}
}